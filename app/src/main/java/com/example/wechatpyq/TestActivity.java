package com.example.wechatpyq;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Browser;
import android.text.Editable;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.TextPaint;
import android.text.style.ClickableSpan;
import android.text.style.ForegroundColorSpan;
import android.text.style.StyleSpan;
import android.text.style.URLSpan;
import android.text.util.Linkify;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.time.format.TextStyle;

public class TestActivity extends AppCompatActivity {
    private TextView tv;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test);
        init();
        setspaint();
    }

    private void setspaint() {
        String s = tv.getText().toString();
        String name="涂鸦";
        SpannableStringBuilder style=new SpannableStringBuilder(s);
        style.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.colorname)),0,name.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
//        style.setSpan(new StyleSpan(Typeface.BOLD), 0, name.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

        //这句很重要，也可以添加自定义正则表达式
        Linkify.addLinks(style,Linkify.ALL);
        //主要是获取span的位置
        URLSpan[] spans = style.getSpans(0, s.length(), URLSpan.class);
        URLSpanNoUnderline urlSpanNoUnderline = new URLSpanNoUnderline(spans[0].getURL());
        //这里可以用过循环处理就可以动态实现文本颜色的差别化了
        //设置高亮样式一
        int spanStart = style.getSpanStart(spans[0]);
        int spanEnd = style.getSpanEnd(spans[0]);
//        style.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.colorname)), spanStart, spanEnd, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        style.setSpan(urlSpanNoUnderline, spanStart, spanEnd, 0);
        tv.setText(style);
    }

    private void init() {
        tv=findViewById(R.id.test_tv);
    }

    public void qwq(View view) {
        if (tv.getText().toString().isEmpty()) {
            Toast.makeText(this, "请不要为空", Toast.LENGTH_SHORT).show();
            return;
        }
        EditText et=findViewById(R.id.test_et);
        Editable text = et.getText();
        tv.setText(text);
        setspaint();
    }


    public class URLSpanNoUnderline extends ClickableSpan {
        private final String mURL;
        public URLSpanNoUnderline(String url) {
            mURL = url;
        }

        public String getURL() {
            return mURL;
        }

        @Override

        public void onClick(View widget) {
            Uri uri = (Uri) widget.getTag();
            if(uri!=null) {
                final Context context = widget.getContext();
                Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.putExtra(Browser.EXTRA_APPLICATION_ID, context.getPackageName());
                context.startActivity(intent);
            }

        }

        @Override
        public void updateDrawState(TextPaint ds) {
            super.updateDrawState(ds);
            ds.setUnderlineText(false);
            ds.setColor(getResources().getColor(R.color.colorname));
            //指定文字颜色
//            ds.setTextSize(Color.parseColor("#00ffff"));
        }
    }
}
